FROM golang:1.8.1-alpine

LABEL Name=golang-builder Version=0.0.1 
MAINTAINER Sanghyun Park <sh@baxang.com>

RUN apk add --update-cache build-base musl-dev musl-utils tar sudo \
        && rm -rf /var/cache/apk/*
